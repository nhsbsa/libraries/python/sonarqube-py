import pytest
import os
import shutil
from sonarqube import parser
from sonarqube.community import SonarQube
from sonarqube.exceptions import CliException

@pytest.fixture
def copier(tmpdir, request, monkeypatch):
    filename = request.module.__file__
    test_dir, _ = os.path.splitext(filename)

    if os.path.isdir(test_dir):
        for file in request.param:
            file_location=f"{test_dir}/{file}"
            if os.path.isfile(file_location):
                shutil.copy(file_location, tmpdir)
    
    monkeypatch.chdir(tmpdir)
    return tmpdir


class TestParser:

    def setup_method(self):
        self.suffix = "branch"
        self.sq = SonarQube()

    @pytest.mark.parametrize('copier', [['valid-with-alm-and-monorepo.yml']], indirect=True)
    def test_complete_yaml_with_alm_should_return_project(self, copier):

        #given
        config_file=str(copier.join("valid-with-alm-and-monorepo.yml"))
        #when
        result = parser.read_project(file = str(config_file), suffix = self.suffix, sq = self.sq)
        #then
        assert result.key == 'test.project.key:project'
        assert result.name == 'project'
        assert result.quality_gate == 'Sonar way'
        assert result.quality_profiles['py'] == 'NHSBSA way'
        assert result.quality_profiles['css'] == 'GDS way'
        assert result.alm_profile == 'NHSBSA_GITHUB'
        assert result.monorepo == 'true'

    @pytest.mark.parametrize('copier', [['valid.yml']], indirect=True)
    def test_complete_yaml_should_return_project(self, copier):

        #given
        config_file=str(copier.join("valid.yml"))
        #when
        result = parser.read_project(file = str(config_file), suffix = self.suffix, sq = self.sq)
        #then
        assert result.key == 'test.project.key:project'
        assert result.name == 'project'
        assert result.quality_gate == 'Sonar way'
        assert result.quality_profiles['py'] == 'NHSBSA way'
        assert result.quality_profiles['css'] == 'GDS way'
        assert result.alm_profile == 'NHSBSA_GITLAB'
        assert result.monorepo == 'false'

    @pytest.mark.parametrize('copier', [['duplicate.yml']], indirect=True)
    def test_duplicate_yml_uses_last_defined_value(self, copier):
        #Should this be rejected as malformed?
        #given
        config_file=str(copier.join("duplicate.yml"))
        #when
        result = parser.read_project(file = config_file, suffix = self.suffix, sq = self.sq)
        #then
        assert result.key == 'other.project.key:other'
        assert result.quality_profiles['css'] == 'Another way'

    @pytest.mark.parametrize('copier', [['sonar-project-with-alm-and-monorepo.properties']], indirect=True)
    def test_complete_properties_with_alm_should_return_project(self, copier):

        #given
        config_file=str(copier.join("sonar-project-with-alm-and-monorepo.properties"))
        #when
        result = parser.read_project(file = config_file, suffix = self.suffix, sq = self.sq)
        #then
        assert result.key == 'test.project.key:project-props'
        assert result.name == 'project-props'
        assert result.quality_gate == 'Sonar way props'
        assert result.quality_profiles['py'] == 'NHSBSA way props'
        assert result.quality_profiles['css'] == 'GDS way props'
        assert result.alm_profile == 'NHSBSA_GITHUB'
        assert result.monorepo == 'true'

    @pytest.mark.parametrize('copier', [['sonar-project.properties']], indirect=True)
    def test_complete_properties_should_return_project(self, copier):

        #given
        config_file=str(copier.join("sonar-project.properties"))
        #when
        result = parser.read_project(file = config_file, suffix = self.suffix, sq = self.sq)
        #then
        assert result.key == 'test.project.key:project-props'
        assert result.name == 'project-props'
        assert result.quality_gate == 'Sonar way props'
        assert result.quality_profiles['py'] == 'NHSBSA way props'
        assert result.quality_profiles['css'] == 'GDS way props'
        assert result.alm_profile == 'NHSBSA_GITLAB'
        assert result.monorepo == 'false'

    @pytest.mark.parametrize('copier', [['.sonarqube.yml','sonar-project.properties']], indirect=True)
    def test_unspecified_file_should_use_props_when_props_exists(self, copier):
        
        #given
        #default files have been copied by datadir fixture
        #when
        result = parser.read_project(suffix=self.suffix, sq=self.sq)
        #then
        assert result.key == 'test.project.key:project-props'
        assert result.name == 'project-props'
        assert result.quality_gate == 'Sonar way props'
        assert result.quality_profiles['py'] == 'NHSBSA way props'
        assert result.quality_profiles['css'] == 'GDS way props'

    @pytest.mark.parametrize('copier', [['.sonarqube.yml']], indirect=True)
    def test_unspecified_file_should_use_yaml_when_not_props_exists_and_yaml_exists(self, copier):

        #given
        #default properties file has been copied by datadir fixture
        #when
        result = parser.read_project(suffix=self.suffix, sq=self.sq)
        #then
        assert result.key == 'test.project.key:project-yaml'
        assert result.name == 'project-yaml'
        assert result.quality_gate == 'Sonar way yaml'
        assert result.quality_profiles['py'] == 'NHSBSA way yaml'
        assert result.quality_profiles['css'] == 'GDS way yaml'

    @pytest.mark.parametrize('copier', [[]], indirect=True)
    def test_unspecified_file_should_raise_exception_when_defaults_missing(self, copier):

        #given
        #no files have been copied by datadir fixture
        #when, then
        with pytest.raises(CliException):
            parser.read_project(suffix=self.suffix, sq=self.sq)

    @pytest.mark.parametrize('copier', [['.sonarqube.yml','sonar-project.properties']], indirect=True)
    def test_missing_specified_config_should_raise_exception(self, copier):

        #given
        #defaults have been copied by datadir fixture
        #when, then
        with pytest.raises(CliException):
            parser.read_project(file = "not-there.yml", suffix = self.suffix, sq = self.sq)

    @pytest.mark.parametrize('copier', [['valid-with-wrong.extension']], indirect=True)
    def test_invalid_extension_should_raise_exception(self, copier):

        #given
        config_file=str(copier.join("valid-with-wrong.extension"))
        #when, then
        with pytest.raises(CliException):
            parser.read_project(config_file, self.suffix, self.sq)

    @pytest.mark.parametrize('copier', [['missing-item.yml']], indirect=True)
    def test_missing_yaml_should_raise_exception(self, copier):

        #given
        config_file=str(copier.join("missing-item.yml"))
        #when, then
        with pytest.raises(CliException):
            parser.read_project(file = config_file, suffix = self.suffix, sq = self.sq)

    @pytest.mark.parametrize('copier', [['invalid.yml']], indirect=True)
    def test_invalid_yaml_should_raise_exception(self, copier):

        #given
        config_file=str(copier.join("invalid.yml"))
        #when, then
        with pytest.raises(CliException):
            parser.read_project(file = config_file, suffix = self.suffix, sq = self.sq)

    @pytest.mark.parametrize('copier', [['duplicate.properties']], indirect=True)
    def test_duplicate_properties_should_raise_exception(self, copier):

        #given
        config_file=str(copier.join("duplicate.properties"))
        #when, then
        with pytest.raises(CliException):
            parser.read_project(file = config_file, suffix = self.suffix, sq = self.sq)

    @pytest.mark.parametrize('copier', [['missing.properties']], indirect=True)
    def test_missing_properties_should_raise_exception(self, copier):

        #given
        config_file=str(copier.join("missing.properties"))
        #when, then
        with pytest.raises(CliException):
            parser.read_project(file = config_file, suffix = self.suffix, sq = self.sq)

    @pytest.mark.parametrize('copier', [['invalid.properties']], indirect=True)
    def test_invalid_properties_should_raise_exception(self, copier):

        #given
        config_file=str(copier.join("invalid.properties"))
        #when, then
        with pytest.raises(CliException):
            parser.read_project(file = config_file, suffix = self.suffix, sq = self.sq)