FROM python:3.12-alpine3.18
ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PYTHONPATH=/app \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.6.1 \
  TZ="Europe/London"

# allow interactive terminal with bash
RUN apk update && apk add bash git

# System deps:
RUN pip install "poetry==$POETRY_VERSION"

# Copy only requirements to cache them in docker layer
WORKDIR /app
COPY poetry.lock pyproject.toml /app/
COPY sonarqube /app/sonarqube

# # Project initialization:
RUN poetry config virtualenvs.create false \
  && poetry install --no-interaction --no-ansi --only main

WORKDIR /src

ENTRYPOINT ["python", "-m" , "sonarqube.cli"]
CMD ["--help"]
